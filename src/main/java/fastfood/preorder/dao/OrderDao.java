package fastfood.preorder.dao;

import fastfood.preorder.db.HibernateSessionFactory;
import fastfood.preorder.db.Order;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class OrderDao {
    public Order findById(int id) {
        return HibernateSessionFactory.getSession().get(Order.class, id);
    }

    public void save(Order order) {
        Session session = HibernateSessionFactory.getSession();
        Transaction tx1 = session.beginTransaction();
        session.save(order);
        tx1.commit();
    }

    public void update(Order order) {
        Session session = HibernateSessionFactory.getSession();
        Transaction tx1 = session.beginTransaction();
        session.update(order);
        tx1.commit();
    }

    public void delete(Order order) {
        Session session = HibernateSessionFactory.getSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(order);
        tx1.commit();
    }

    public List<Order> findAll() {
        List<Order> orders = (List<Order>) HibernateSessionFactory
                .getSession()
                .createQuery("From Order")
                .list();
        return orders;
    }
}
