package fastfood.preorder.dao;

import fastfood.preorder.db.HibernateSessionFactory;
import fastfood.preorder.db.MenuItem;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class MenuItemDao {
    public MenuItem findById(int id) {
        return HibernateSessionFactory.getSession().get(MenuItem.class, id);
    }

    public void save(MenuItem menuItem) {
        Session session = HibernateSessionFactory.getSession();
        Transaction tx1 = session.beginTransaction();
        session.save(menuItem);
        tx1.commit();
    }

    public void update(MenuItem menuItem) {
        Session session = HibernateSessionFactory.getSession();
        Transaction tx1 = session.beginTransaction();
        session.update(menuItem);
        tx1.commit();
    }

    public void delete(MenuItem menuItem) {
        Session session = HibernateSessionFactory.getSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(menuItem);
        tx1.commit();
    }

    public List<MenuItem> findAll() {
        List<MenuItem> menuItems = (List<MenuItem>) HibernateSessionFactory
                .getSession()
                .createQuery("From MenuItem")
                .list();
        return menuItems;
    }
}
