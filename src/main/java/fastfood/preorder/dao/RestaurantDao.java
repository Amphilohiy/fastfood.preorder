package fastfood.preorder.dao;

import fastfood.preorder.db.HibernateSessionFactory;
import fastfood.preorder.db.Order;
import fastfood.preorder.db.Restaurant;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.GregorianCalendar;
import java.util.List;

public class RestaurantDao {
    public Restaurant findById(int id) {
        return HibernateSessionFactory.getSession().get(Restaurant.class, id);
    }

    public void save(Restaurant restaurant) {
        Session session = HibernateSessionFactory.getSession();
        Transaction tx1 = session.beginTransaction();
        session.save(restaurant);
        tx1.commit();
    }

    public void update(Restaurant restaurant) {
        Session session = HibernateSessionFactory.getSession();
        Transaction tx1 = session.beginTransaction();
        session.update(restaurant);
        tx1.commit();
    }

    public void delete(Restaurant restaurant) {
        Session session = HibernateSessionFactory.getSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(restaurant);
        tx1.commit();
    }

    public List<Restaurant> findAll() {
        List<Restaurant> restaurants = (List<Restaurant>) HibernateSessionFactory
                .getSession()
                .createQuery("From Restaurant")
                .list();
        return restaurants;
    }

    public List<Order> findOrdersForDay(Restaurant restaurant, GregorianCalendar calendar){
        // @TODO not working, if restaurant works past midnight!
        GregorianCalendar opening = (GregorianCalendar)calendar.clone();
        opening.set(GregorianCalendar.HOUR_OF_DAY, restaurant.getOpensAt().getHour());
        opening.set(GregorianCalendar.MINUTE, restaurant.getOpensAt().getMinute());
        opening.set(GregorianCalendar.SECOND, 0);
        opening.set(GregorianCalendar.MILLISECOND, 0);

        GregorianCalendar closing = (GregorianCalendar)calendar.clone();
        closing.set(GregorianCalendar.HOUR_OF_DAY, restaurant.getClosesAt().getHour());
        closing.set(GregorianCalendar.MINUTE, restaurant.getClosesAt().getMinute());
        closing.set(GregorianCalendar.SECOND, 0);
        closing.set(GregorianCalendar.MILLISECOND, 0);

        List<Order> orders = (List<Order>) HibernateSessionFactory
                .getSession()
                .createQuery("FROM Order WHERE restaurant_id = :r_id AND starts_at >= :start_of_day AND starts_at <= :end_of_day AND canceled IS :canceled ORDER BY starts_at")
                .setParameter("r_id", restaurant.getId())
                .setParameter("start_of_day", opening)
                .setParameter("canceled", false)
                .setParameter("end_of_day", closing)
                .list();
        return orders;
    }
}
