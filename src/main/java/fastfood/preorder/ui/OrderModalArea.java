package fastfood.preorder.ui;

import org.eclipse.swt.widgets.Composite;

/**
 * Area, representing step of order creation. Used in StackLayout and controls availability of "Next" button.
 */
public abstract class OrderModalArea extends Composite {
    CreateOrderModal modal;

    OrderModalArea(Composite parent, int style){
        super(parent, style);
    }

    /**
     * Abstract method, defines next button state.
     * @return if next button is enabled.
     */
    public abstract boolean getNextButtonAbility();
}
