package fastfood.preorder.ui;

import fastfood.preorder.Main;
import fastfood.preorder.TimeUtil;
import fastfood.preorder.db.Order;
import fastfood.preorder.db.Restaurant;
import fastfood.preorder.service.RestaurantService;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Area for choosing restaurant.
 */
public class ChooseRestaurantArea extends OrderModalArea {
    private RestaurantService restaurantService = new RestaurantService();
    private Order order;

    public ChooseRestaurantArea(Composite parent, CreateOrderModal modal, Order order){
        super(parent, SWT.NONE);
        this.order = order;

        this.setLayout(new GridLayout(1, true));

        ListViewer restaurantsList = new ListViewer(this, SWT.BORDER | SWT.SINGLE | SWT.V_SCROLL);
        restaurantsList.setContentProvider(new IStructuredContentProvider() {
            @Override
            public Object[] getElements(Object o) {
                return ((java.util.List<Restaurant>)o).toArray();
            }

            @Override
            public void dispose() {}

            @Override
            public void inputChanged(Viewer viewer, Object o, Object o1) {}
        });
        restaurantsList.setLabelProvider(new LabelProvider() {
            public Image getImage(Object element) {
                return null;
            }

            public String getText(Object element) {
                Restaurant restaurant = ((Restaurant)element);
                return restaurant.getAddress() + " (" +

                        Main.timeFormat.format(TimeUtil.toDate(restaurant.getOpensAt())) + "-" +
                        Main.timeFormat.format(TimeUtil.toDate(restaurant.getClosesAt())) + ")";
            }
        });
        restaurantsList.getControl().addListener(SWT.Selection, e -> {
            order.setRestaurant((Restaurant) restaurantsList.getStructuredSelection().getFirstElement());
            modal.setNextButtonEnabled(order.getRestaurant() != null);
        });
        restaurantsList.getControl().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        restaurantsList.setInput(restaurantService.findAllRestaurants());
    }

    @Override
    public boolean getNextButtonAbility() {
        return order.getRestaurant() != null;
    }
}
