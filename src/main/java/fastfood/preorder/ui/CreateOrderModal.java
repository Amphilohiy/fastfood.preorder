package fastfood.preorder.ui;

import fastfood.preorder.db.Order;
import fastfood.preorder.service.OrderService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.*;

public class CreateOrderModal {
    private Shell shell;
    private Display dipslay;

    private OrderModalArea[] controls;
    private int currentControlIndex = 0;

    private StackLayout stackLayout;
    private Composite workspace;
    private Button previous;
    private Button next;

    private Order order;

    public CreateOrderModal(Display display){
        this.dipslay = display;

        shell = new Shell(display, SWT.CLOSE | SWT.TITLE | SWT.BORDER | SWT.OK | SWT.APPLICATION_MODAL);
        shell.setLayout(new GridLayout(1, true));
        shell.setText("Оформление предзаказа");
        order = new Order();

        stackLayout = new StackLayout();
        workspace = new Composite(shell, SWT.NONE);
        workspace.setLayout(stackLayout);
        workspace.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

        Composite buttonsBar = new Composite(shell, SWT.RIGHT_TO_LEFT);
        buttonsBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        buttonsBar.setLayout(new RowLayout());

        controls = new OrderModalArea[]{
                new ChooseRestaurantArea(workspace, this, order),
                new ChooseItemsArea(workspace, this, order),
                new ChooseTimeArea(workspace, this, order)
        };

        stackLayout.topControl = controls[0];

        next = new Button(buttonsBar, SWT.NONE);
        next.addListener(SWT.Selection, e-> nextArea());
        next.setText("Завершить");
        previous = new Button(buttonsBar, SWT.NONE);
        previous.addListener(SWT.Selection, e -> prevArea());
        previous.setText("Назад");

        shell.pack();
        shell.setSize(500, shell.getSize().y);
        refreshButtons();
    }

    public void setNextButtonEnabled(boolean enabled){
        next.setEnabled(enabled);
    }

    public void execute(){
        shell.open();
        while (!shell.isDisposed()) {
            if (!dipslay.readAndDispatch()) {
                dipslay.sleep();
            }
        }
    }

    private void nextArea(){
        if (currentControlIndex < controls.length - 1){
            currentControlIndex++;
            stackLayout.topControl = controls[currentControlIndex];
            workspace.layout();
            refreshButtons();
        } else {
            finish();
        }
    }

    private void prevArea(){
        currentControlIndex--;
        stackLayout.topControl = controls[currentControlIndex];
        workspace.layout();
        refreshButtons();
    }

    private void refreshButtons(){
        previous.setVisible(currentControlIndex != 0);
        if (currentControlIndex == controls.length - 1) next.setText("Завершить"); else next.setText("Далее");
        next.setEnabled(controls[currentControlIndex].getNextButtonAbility());
    }

    protected void finish(){
        new OrderService().saveOrder(order);
        shell.close();
    }
}
