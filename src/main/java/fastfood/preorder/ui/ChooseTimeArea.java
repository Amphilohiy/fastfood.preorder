package fastfood.preorder.ui;

import fastfood.preorder.TimeSlot;
import fastfood.preorder.db.Order;
import fastfood.preorder.service.RestaurantService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.*;

import java.util.GregorianCalendar;

public class ChooseTimeArea extends OrderModalArea {
    private CreateOrderModal modal;

    private DateTime calendar;
    private DateTime time;
    private Button search;
    private Composite summaryArea;
    private Label summaryRestaurant;
    private Label summaryOrderPrice;
    private Label summaryDate;

    private RestaurantService restaurantService = new RestaurantService();

    private Order order;

    public ChooseTimeArea(Composite parent, CreateOrderModal modal, Order order){
        super(parent, SWT.NONE);
        this.modal = modal;
        this.order = order;

        this.setLayout(new GridLayout(2, false));

        Composite dateWorkspace = new Composite(this, SWT.NONE);
        dateWorkspace.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true));
        dateWorkspace.setLayout(new GridLayout(1, true));

        calendar = new DateTime(dateWorkspace, SWT.CALENDAR);
        calendar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));

        time = new DateTime(dateWorkspace, SWT.TIME);
        time.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));

        summaryArea = new Composite(this, SWT.NONE);
        summaryArea.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        RowLayout summaryAreaLayout = new RowLayout(SWT.VERTICAL);
        summaryAreaLayout.justify = true;
        summaryArea.setLayout(summaryAreaLayout);

        // should be order items list as well
        summaryRestaurant = new Label(summaryArea, SWT.NONE);
        summaryOrderPrice = new Label(summaryArea, SWT.NONE);
        summaryDate = new Label(summaryArea, SWT.NONE);
        summaryArea.pack();

        search = new Button(this, SWT.NONE);
        search.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));
        search.setText("Найти");
        search.addListener(SWT.Selection, (Event e)-> searchTimeWindows() );

        this.pack();
    }

    public void searchTimeWindows(){
        GregorianCalendar chosenDate = new GregorianCalendar(
                calendar.getYear(),
                calendar.getMonth(),
                calendar.getDay(),
                time.getHours(),
                time.getMinutes()
        );
        TimeSlot timeSlot = restaurantService.getTimeSlot(order.getRestaurant(), chosenDate, order.getCookingTime(), new GregorianCalendar());

        if (timeSlot != null){
            order.setStartsAt(timeSlot.getStarts());
            order.setFinishedAt(timeSlot.getFinishes());
            modal.setNextButtonEnabled(true);

            summaryRestaurant.setText(order.getRestaurant().getAddress());
            summaryOrderPrice.setText("Сумма: " + order.getPrice() + "р.");
            summaryDate.setText("Будет готов к " + order.getFinishedAt().get(GregorianCalendar.HOUR_OF_DAY) + ":"
                    + order.getFinishedAt().get(GregorianCalendar.MINUTE));

            summaryArea.layout(true);
        } else {
            order.setStartsAt(null);
            order.setFinishedAt(null);
            modal.setNextButtonEnabled(false);

            summaryRestaurant.setText("На данную дату заказ");
            summaryOrderPrice.setText("не может быть");
            summaryDate.setText("приготовлен!");

            summaryArea.layout(true);
        }
    }

    @Override
    public boolean getNextButtonAbility() {
        return order.getStartsAt() != null && order.getFinishedAt() != null;
    }
}