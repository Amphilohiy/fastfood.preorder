package fastfood.preorder.ui;

import fastfood.preorder.db.MenuItem;
import fastfood.preorder.db.Order;
import fastfood.preorder.db.OrderMenuItem;
import fastfood.preorder.service.MenuItemService;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Area for assembling items in order.
 */
public class ChooseItemsArea extends OrderModalArea {
    private MenuItemService menuItemsService = new MenuItemService();

    private Order order;

    private ListViewer orderList;
    private Label totalCost;
    private Label totalTime;
    private CreateOrderModal modal;

    private static DecimalFormat df = new DecimalFormat("#.##");

    public ChooseItemsArea(Composite parent, CreateOrderModal modal, Order order){
        super(parent, SWT.NONE);
        this.order = order;
        this.modal = modal;
        df.setRoundingMode(RoundingMode.HALF_UP);

        this.setLayout(new GridLayout(3, false));
        this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

        final Composite orderWorkspace = new Composite(this, SWT.NONE);
        orderWorkspace.setLayout(new GridLayout(2, false));
        orderWorkspace.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

        // Menu item list
        ListViewer menuItemsList = new ListViewer(orderWorkspace, SWT.BORDER | SWT.SINGLE | SWT.V_SCROLL);
        menuItemsList.setContentProvider(new IStructuredContentProvider() {
            @Override
            public Object[] getElements(Object o) {
                return ((java.util.List<MenuItem>)o).toArray();
            }

            @Override
            public void dispose() {}

            @Override
            public void inputChanged(Viewer viewer, Object o, Object o1) {}
        });
        menuItemsList.setLabelProvider(new LabelProvider() {
            public Image getImage(Object element) {
                return null;
            }

            public String getText(Object element) {
                return ((MenuItem)element).getName() + "(" + ((MenuItem) element).getPrice() + " р.)";
            }
        });
        menuItemsList.getControl().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        java.util.List<MenuItem> menuItems = menuItemsService.findAllMenuItems();
        menuItemsList.setInput(menuItems);

        // Order list
        orderList = new ListViewer(orderWorkspace, SWT.BORDER | SWT.SINGLE | SWT.V_SCROLL);
        orderList.setContentProvider(new IStructuredContentProvider() {
            @Override
            public Object[] getElements(Object o) {
                return ((java.util.List<OrderMenuItem>)o).toArray();
            }

            @Override
            public void dispose() {}

            @Override
            public void inputChanged(Viewer viewer, Object o, Object o1) {}
        });
        orderList.setLabelProvider(new LabelProvider() {
            public Image getImage(Object element) {
                return null;
            }

            public String getText(Object element) {
                return ((OrderMenuItem)element).getMenuItem().getName() +
                        "(" + ((OrderMenuItem) element).getQuantity() + " шт.)";
            }
        });
        orderList.getControl().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

        orderList.setInput(order.getOrderMenuItems());

        Composite buttonWorkspace = new Composite(orderWorkspace, SWT.NONE);
        buttonWorkspace.setLayout(new FillLayout(SWT.VERTICAL));
        buttonWorkspace.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

        Button addOrder = new Button(buttonWorkspace, SWT.NONE);
        addOrder.setText("Добавить в корзину");
        addOrder.addListener(SWT.Selection, (Event e)->{
            for (Iterator menuItemIter = menuItemsList.getStructuredSelection().iterator(); menuItemIter.hasNext();){
                MenuItem menuItem = (MenuItem)menuItemIter.next();
                Boolean alreadyInList = false;
                for (OrderMenuItem orderMenuItem : order.getOrderMenuItems()){
                    if (orderMenuItem.getMenuItem().getId().equals(menuItem.getId())){
                        alreadyInList = true;
                        // @TODO FIX AMOUNT ADDING (ADD NUMBER FIELD)
                        orderMenuItem.setQuantity(orderMenuItem.getQuantity() + 1);
                    }
                }
                if (!alreadyInList){
                    OrderMenuItem orderMenuItem = new OrderMenuItem();
                    orderMenuItem.setOrder(order);
                    orderMenuItem.setMenuItem(menuItem);
                    // @TODO FIX AMOUNT ADDING (ADD NUMBER FIELD)
                    orderMenuItem.setQuantity(1);

                    order.getOrderMenuItems().add(orderMenuItem);
                }
            }
            updateTotal(true);
        });

        Button removeOrder = new Button(buttonWorkspace, SWT.NONE);
        removeOrder.setText("Убрать из корзины");
        removeOrder.addListener(SWT.Selection, (Event e)->{
            // @TODO MAYBE SEARCH FOR SINGLE LINKED LIST?
            List<OrderMenuItem> deletionList = new LinkedList<>();
            for (Iterator orderMenuItemIter = orderList.getStructuredSelection().iterator(); orderMenuItemIter.hasNext();){
                OrderMenuItem orderMenuItem = (OrderMenuItem)orderMenuItemIter.next();
                // @TODO FIX AMOUNT SUBTRACTING (ADD NUMBER FIELD)
                if (orderMenuItem.getQuantity() > 1) {
                    orderMenuItem.setQuantity(orderMenuItem.getQuantity() - 1);
                } else {
                    deletionList.add(orderMenuItem);
                }
            }
            order.getOrderMenuItems().removeAll(deletionList);
            updateTotal(true);
        });

        Composite totalWorkspace = new Composite(orderWorkspace, SWT.NONE);
        totalWorkspace.setLayout(new FillLayout(SWT.VERTICAL));
        totalWorkspace.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

        totalCost = new Label(totalWorkspace, SWT.NONE);
        totalTime = new Label(totalWorkspace, SWT.NONE);

        updateTotal(false);
    }

    void updateTotal(boolean setButtonAbility){
        Double cost = order.getPrice();
        Integer time = order.getCookingTime();
        totalCost.setText("Общая сумма: " + df.format(cost) + "р.");
        if (time >= 60){
            totalTime.setText("Время приготовления: " + time / 60 + "ч. " + time % 60 + "мин.");
        } else {
            totalTime.setText("Время приготовления: " + time + "мин.");
        }

        orderList.refresh();
        if (setButtonAbility) modal.setNextButtonEnabled(order.getOrderMenuItems().size() > 0);
    }

    @Override
    public boolean getNextButtonAbility() {
        return order.getOrderMenuItems().size() > 0;
    }
}