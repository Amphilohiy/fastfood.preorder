package fastfood.preorder.service;

import fastfood.preorder.dao.MenuItemDao;
import fastfood.preorder.db.MenuItem;

import java.util.List;

public class MenuItemService {
    private MenuItemDao menuItemsDao = new MenuItemDao();

    public MenuItemService() {
    }

    public MenuItem findMenuItem(int id) {
        return menuItemsDao.findById(id);
    }

    public void saveMenuItem(MenuItem menuItem) {
        menuItemsDao.save(menuItem);
    }

    public void deleteMenuItem(MenuItem menuItem) {
        menuItemsDao.delete(menuItem);
    }

    public void updateMenuItem(MenuItem menuItem) {
        menuItemsDao.update(menuItem);
    }

    public List<MenuItem> findAllMenuItems() {
        return menuItemsDao.findAll();
    }
}
