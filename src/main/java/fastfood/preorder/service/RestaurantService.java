package fastfood.preorder.service;

import fastfood.preorder.TimeSlot;
import fastfood.preorder.dao.RestaurantDao;
import fastfood.preorder.db.Order;
import fastfood.preorder.db.Restaurant;

import java.util.GregorianCalendar;
import java.util.List;

public class RestaurantService {
    private RestaurantDao restaurantsDao = new RestaurantDao();

    public RestaurantService() {
    }

    public Restaurant findRestaurant(int id) {
        return restaurantsDao.findById(id);
    }

    public void saveRestaurant(Restaurant restaurant) {
        restaurantsDao.save(restaurant);
    }

    public void deleteRestaurant(Restaurant restaurant) {
        restaurantsDao.delete(restaurant);
    }

    public void updateRestaurant(Restaurant restaurant) {
        restaurantsDao.update(restaurant);
    }

    public List<Restaurant> findAllRestaurants() {
        return restaurantsDao.findAll();
    }

    public List<Order> findOrdersForDay(Restaurant restaurant, GregorianCalendar calendar){
        return restaurantsDao.findOrdersForDay(restaurant, calendar);
    }

    /**
     * Gets available time slots for order
     * TODO search time slot before overlapping order (giving possible alternative to eat colder order)
     * @param restaurant restaurant in which order should be prepared
     * @param calendar time when order should be ready
     * @param duration time needed to prepare order
     * @param now pass current moment
     * @return
     */
    public TimeSlot getTimeSlot(Restaurant restaurant, GregorianCalendar calendar, int duration, GregorianCalendar now){
        List<Order> orders = findOrdersForDay(restaurant, calendar);

        // Restaurant schedule for day
        TimeSlot restaurantSchedule = new TimeSlot(new GregorianCalendar(
                calendar.get(GregorianCalendar.YEAR),
                calendar.get(GregorianCalendar.MONTH),
                calendar.get(GregorianCalendar.DAY_OF_MONTH),
                restaurant.getOpensAt().getHour(),
                restaurant.getOpensAt().getMinute()
        ), new GregorianCalendar(
                calendar.get(GregorianCalendar.YEAR),
                calendar.get(GregorianCalendar.MONTH),
                calendar.get(GregorianCalendar.DAY_OF_MONTH),
                restaurant.getClosesAt().getHour(),
                restaurant.getClosesAt().getMinute()
        ));

        // offsetting calendar by preparation time (as we want order be ready by that time)...
        calendar = (GregorianCalendar)calendar.clone();
        calendar.add(GregorianCalendar.MINUTE, -1*duration);
        // ... and limiting starting time by current date
        if (now.after(calendar)) calendar = now;

        TimeSlot desiredSlot = new TimeSlot(calendar, duration);

        // offsetting calendar by restaurant opening
        if (desiredSlot.getStarts().before(restaurantSchedule.getStarts())){
            desiredSlot = new TimeSlot(desiredSlot.getStarts(), desiredSlot.getFinishes());
        }

        // Since orders ordered by time we can just iterate over
        for (Order order : orders){
            // if orders overlap then we try place our order after existing
            if (desiredSlot.isOverlap(order.getStartsAt(), order.getFinishedAt())) {
                desiredSlot = new TimeSlot(order.getFinishedAt(), duration);
            }
        }

        // checking if restaurant closing by time order should be prepared
        if (restaurantSchedule.getFinishes().before(desiredSlot.getFinishes())){
            return null;
        }
        return desiredSlot;
    }
}
