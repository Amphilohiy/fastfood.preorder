package fastfood.preorder.db;

import fastfood.preorder.service.MenuItemService;
import fastfood.preorder.service.RestaurantService;

import java.time.LocalTime;

public class DBPopulator {
    private static Boolean populated = false;

    public static void populate(){
        if (populated) return;
        RestaurantService restaurants = new RestaurantService();
        MenuItemService menuItems = new MenuItemService();

        // restaurants
        Restaurant restaurant = new Restaurant();
        restaurant.setAddress("Фрунзе, 67/1 к1 киоск");
        restaurant.setOpensAt(LocalTime.of(7, 0));
        restaurant.setClosesAt(LocalTime.of(21, 0));
        restaurants.saveRestaurant(restaurant);

        restaurant = new Restaurant();
        restaurant.setAddress("Селезнёва, 46/1");
        restaurant.setOpensAt(LocalTime.of(7, 0));
        restaurant.setClosesAt(LocalTime.of(21, 0));
        restaurants.saveRestaurant(restaurant);

        restaurant = new Restaurant();
        restaurant.setAddress("Карла Маркса проспект, 20 к7");
        restaurant.setOpensAt(LocalTime.of(8, 0));
        restaurant.setClosesAt(LocalTime.of(22, 0));
        restaurants.saveRestaurant(restaurant);

        // menu items
        MenuItem menuItem = new MenuItem();
        menuItem.setName("Шурама");
        menuItem.setCookingTime(10);
        menuItem.setPrice(180.0);
        menuItems.saveMenuItem(menuItem);

        menuItem = new MenuItem();
        menuItem.setName("Пицца");
        menuItem.setCookingTime(30);
        menuItem.setPrice(700.0);
        menuItems.saveMenuItem(menuItem);

        menuItem = new MenuItem();
        menuItem.setName("Бургер");
        menuItem.setCookingTime(15);
        menuItem.setPrice(200.0);
        menuItems.saveMenuItem(menuItem);

        menuItem = new MenuItem();
        menuItem.setName("Кофе");
        menuItem.setCookingTime(5);
        menuItem.setPrice(50.0);
        menuItems.saveMenuItem(menuItem);

        populated = true;
    }
}
