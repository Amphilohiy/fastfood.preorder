package fastfood.preorder.db;

import javax.persistence.*;

@Entity
@Table(name="orders_menu_items")
public class OrderMenuItem {
    @Id
    @SequenceGenerator(name = "OrderMenuItem_SEQ", sequenceName = "order_menu_item_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "OrderMenuItem_SEQ", strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id")
    private Order order;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "menu_item_id")
    private MenuItem menuItem;

    private Integer quantity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public MenuItem getMenuItem() {
        return menuItem;
    }

    public void setMenuItem(MenuItem menuItem) {
        this.menuItem = menuItem;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getPrice(){
        return menuItem != null ? menuItem.getPrice() * quantity : 0.0;
    }

    public Integer getCookingTime(){
        return menuItem != null ? menuItem.getCookingTime() * quantity : 0;
    }
}
