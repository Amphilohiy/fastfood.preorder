package fastfood.preorder.db;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="menu_items")
public class MenuItem {
    @Id
    @SequenceGenerator(name = "USING_SEQ", sequenceName = "using_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "USING_SEQ", strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "name")
    private String name;

    // Cooking time in minutes
    @Column(name = "cooking_time")
    private Integer cookingTime;

    @Column(name = "price")
    private Double price;

    @OneToMany(mappedBy="menuItem", fetch = FetchType.LAZY)
    private List<OrderMenuItem> orderMenuItems = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCookingTime() {
        return cookingTime;
    }

    public void setCookingTime(Integer cookingTime) {
        this.cookingTime = cookingTime;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public List<OrderMenuItem> getOrderMenuItems() {
        return orderMenuItems;
    }

    public void setOrderMenuItems(List<OrderMenuItem> orderMenuItems) {
        this.orderMenuItems = orderMenuItems;
    }
}
