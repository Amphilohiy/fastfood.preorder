package fastfood.preorder.db;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateSessionFactory {
    private static SessionFactory sessionFactory;
    private static Session session;

    private HibernateSessionFactory() {}

    private static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                Configuration configuration = new Configuration().configure();
                configuration.addAnnotatedClass(MenuItem.class);
                configuration.addAnnotatedClass(Order.class);
                configuration.addAnnotatedClass(Restaurant.class);
                configuration.addAnnotatedClass(OrderMenuItem.class);
                StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
                sessionFactory = configuration.buildSessionFactory(builder.build());
            } catch (Exception e) {
                System.out.println("Error: " + e);
            }
        }
        return sessionFactory;
    }

    public static Session getSession(){
        if (session == null){
            session = getSessionFactory().openSession();
        }
        return session;
    }

    public static void closeSession(){
        if (session != null){
            session.close();
            session = null;
        }
    }
}
