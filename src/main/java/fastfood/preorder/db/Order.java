package fastfood.preorder.db;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

@Entity
@Table(name="orders")
public class Order {
    @Id
    @SequenceGenerator(name = "USING_SEQ", sequenceName = "using_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "USING_SEQ", strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "restaurant_id")
    private Restaurant restaurant;

    @OneToMany(cascade=CascadeType.ALL,mappedBy="order",fetch = FetchType.LAZY)
    private List<OrderMenuItem> orderMenuItems = new ArrayList<>();

    @Column(name="starts_at")
    private GregorianCalendar startsAt;

    @Column(name="finished_at")
    private GregorianCalendar finishedAt;

    @Column(nullable=false)
    private Boolean canceled = false;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public List<OrderMenuItem> getOrderMenuItems() {
        return orderMenuItems;
    }

    public void setOrderMenuItems(List<OrderMenuItem> orderMenuItems) {
        this.orderMenuItems = orderMenuItems;
    }

    public GregorianCalendar getStartsAt() {
        return startsAt;
    }

    public void setStartsAt(GregorianCalendar startsAt) {
        this.startsAt = startsAt;
    }

    public GregorianCalendar getFinishedAt() {
        return finishedAt;
    }

    public void setFinishedAt(GregorianCalendar finishedAt) {
        this.finishedAt = finishedAt;
    }

    public Boolean getCanceled() {
        return canceled;
    }

    public void setCanceled(Boolean canceled) {
        this.canceled = canceled;
    }

    public Double getPrice(){
        return orderMenuItems.stream()
                .map(OrderMenuItem::getPrice)
                .reduce(0.0, (acc, price) -> price + acc);
    }

    public Integer getCookingTime(){
        return orderMenuItems.stream()
                .map(OrderMenuItem::getCookingTime)
                .reduce(0, (acc, price) -> price + acc);
    }
}
