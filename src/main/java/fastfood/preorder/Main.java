package fastfood.preorder;

import fastfood.preorder.db.DBPopulator;
import fastfood.preorder.db.HibernateSessionFactory;
import fastfood.preorder.db.Order;
import fastfood.preorder.service.OrderService;
import fastfood.preorder.ui.CreateOrderModal;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

import java.text.SimpleDateFormat;

public class Main {
    static private SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM");
    static public SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
    static private OrderService orderService = new OrderService();

    public static void main (String [] args) {
        DBPopulator.populate();

        final Display display = new Display();
        final Shell shell = new Shell(display);
        shell.setLayout(new GridLayout(1, false));
        shell.setText("Фастфуд Предзаказ");

        ToolBar toolbar = new ToolBar(shell, SWT.BORDER);
        toolbar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

        TableViewer orderTable = new TableViewer(shell, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);

        ToolItem addOrder = new ToolItem(toolbar, SWT.PUSH);
        addOrder.addListener(SWT.Selection, e -> {
            CreateOrderModal modal = new CreateOrderModal(display);
            modal.execute();
            orderTable.setInput(orderService.findAllOrders());
            orderTable.refresh();
        });
        addOrder.setText("Оформить предзаказ");

        ToolItem disableOrder = new ToolItem(toolbar, SWT.PUSH | SWT.BORDER);
        disableOrder.setEnabled(false);
        disableOrder.setText("Отменить заказ");

        orderTable.getControl().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

        TableViewerColumn colCanceled = new TableViewerColumn(orderTable, SWT.NONE);
        colCanceled.getColumn().setText("Отменен");
        colCanceled.getColumn().setWidth(75);
        colCanceled.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object element) {
                return ((Order) element).getCanceled() ? "X" : "";
            }
        });

        TableViewerColumn colRestaurant = new TableViewerColumn(orderTable, SWT.NONE);
        colRestaurant.getColumn().setText("Ресторан");
        colRestaurant.getColumn().setWidth(300);
        colRestaurant.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object element) {
                return ((Order) element).getRestaurant().getAddress();
            }
        });

        TableViewerColumn colTimeWindow = new TableViewerColumn(orderTable, SWT.NONE);
        colTimeWindow.getColumn().setText("Время приготовления");
        colTimeWindow.getColumn().setWidth(200);
        colTimeWindow.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object element) {
                Order order = ((Order) element);
                return "[" + dateFormat.format(order.getStartsAt().getTime()) + "]: "
                        + timeFormat.format(order.getStartsAt().getTime()) + " - "
                        + timeFormat.format(order.getFinishedAt().getTime());
            }
        });

        TableViewerColumn colItems = new TableViewerColumn(orderTable, SWT.NONE);
        colItems.getColumn().setText("Содержимое");
        colItems.getColumn().setWidth(200);
        colItems.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object element) {
                Order order = ((Order) element);
                return String.join(", ", order.getOrderMenuItems().stream()
                        .map(item -> item.getMenuItem().getName() + " (x" + item.getQuantity() + ")")
                        .toArray(String[]::new));

            }
        });

        orderTable.addSelectionChangedListener((e)->{
            disableOrder.setEnabled(orderTable.getStructuredSelection().size() > 0);
        });
        disableOrder.addListener(SWT.Selection, e -> {
            Order order = (Order)orderTable.getStructuredSelection().getFirstElement();
            order.setCanceled(true);
            orderService.saveOrder(order);
            orderTable.refresh();
        });

        orderTable.setContentProvider(ArrayContentProvider.getInstance());
        orderTable.setInput(orderService.findAllOrders());

        orderTable.getTable().setHeaderVisible(true);
        orderTable.getTable().setLinesVisible(true);
        orderTable.refresh();

        //shell.pack();
        shell.open();
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) display.sleep();
        }
        display.dispose();
        HibernateSessionFactory.closeSession();
    }
}
