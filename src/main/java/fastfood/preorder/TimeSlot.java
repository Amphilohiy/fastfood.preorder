package fastfood.preorder;

import java.util.GregorianCalendar;

/**
 * Helper class to define if time slot overlaps another time slot.
 */
public class TimeSlot{
    private GregorianCalendar starts;
    private GregorianCalendar finishes;
    public TimeSlot(GregorianCalendar starts, int duration){
        this.starts = starts;
        finishes = (GregorianCalendar)starts.clone();
        finishes.add(GregorianCalendar.MINUTE, duration);
    }

    public TimeSlot(GregorianCalendar starts, GregorianCalendar finishes){
        this.starts = starts;
        this.finishes = finishes;
    }

    public GregorianCalendar getStarts() {
        return starts;
    }

    public GregorianCalendar getFinishes() {
        return finishes;
    }

    /**
     * Checks if time slot is overlapping another time slot. Checks 6 cases (number represents index of time slot
     * and s/e represents starting and ending time respectively):
     * 2s - 1s - 2e: current time slot starts in second time slot
     * 2s - 1e - 2e: current time slot finished in second time slot
     * 1s - 2s - 2e - 1e: current time slot contains second time slot
     * 2s - 1s - 1e - 2e: second time slot contains current time slot
     * 1s = 2s: equals starts of time slots
     * 1e = 2e: equals finishes of time slots
     *
     * @param secondStarts
     * @param secondFinishes
     * @return bool telling if time slot overlaps provided time slot
     */
    public boolean isOverlap(GregorianCalendar secondStarts, GregorianCalendar secondFinishes){
        // First case
        return ((starts.after(secondStarts) && starts.before(secondFinishes)) ||
                // Second case
                (finishes.after(secondStarts) && finishes.before(secondFinishes)) ||
                // Third case
                (starts.before(secondStarts) && finishes.after(secondFinishes)) ||
                // Forth case
                (secondStarts.before(starts) && secondFinishes.after(finishes)) ||
                // Equals starts
                starts.equals(secondStarts) ||
                // Equals ends
                finishes.equals(secondFinishes)
        );
    }
}
