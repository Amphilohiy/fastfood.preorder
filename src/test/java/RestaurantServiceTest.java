import fastfood.preorder.TimeSlot;
import fastfood.preorder.db.Order;
import fastfood.preorder.db.Restaurant;
import fastfood.preorder.service.OrderService;
import fastfood.preorder.service.RestaurantService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.time.LocalTime;
import java.util.GregorianCalendar;

@RunWith(BlockJUnit4ClassRunner.class)
@Transactional
public class RestaurantServiceTest {
    private RestaurantService restaurantService = new RestaurantService();
    private OrderService orderService = new OrderService();
    private Restaurant restaurant;

    @Before
    public void initRestaurant(){
        restaurant = new Restaurant();
        restaurant.setAddress("Фрунзе, 67/1 к1 киоск");
        restaurant.setOpensAt(LocalTime.of(7, 0));
        restaurant.setClosesAt(LocalTime.of(21, 0));
        restaurantService.saveRestaurant(restaurant);
    }

    @Test
    public void getTimeSlot(){
        GregorianCalendar now = new GregorianCalendar(2000, 1, 1, 0, 0);

        // simple order at start of the day
        Assert.assertNotNull("Couldn't create time slot at start of the day", restaurantService.getTimeSlot(restaurant, new GregorianCalendar(
                2000, 1, 1, 7, 0
        ), 120, now));

        // simple order tightly making it to the end of day
        Assert.assertNotNull("Couldn't create time slot at end of the day", restaurantService.getTimeSlot(restaurant, new GregorianCalendar(
                2000, 1, 1, 19, 0
        ), 120, now));

        // introducing order and overlapping
        GregorianCalendar orderStarts = new GregorianCalendar(2000, 1, 1, 10, 0);
        GregorianCalendar orderEnds = new GregorianCalendar(2000, 1, 1, 12, 0);

        Order order = new Order();
        order.setRestaurant(restaurant);
        order.setStartsAt(orderStarts);
        order.setFinishedAt(orderEnds);
        orderService.saveOrder(order);

        // End of time slot within order
        TimeSlot timeSlot = restaurantService.getTimeSlot(restaurant, new GregorianCalendar(
                2000, 1, 1, 11, 0
        ), 120, now);
        Assert.assertEquals("End of time slot within order not shifted correctly", timeSlot.getStarts(), orderEnds);

        // Start of time slot within order
        timeSlot = restaurantService.getTimeSlot(restaurant, new GregorianCalendar(
                2000, 1, 1, 13, 0
        ), 120, now);
        Assert.assertEquals("Start of time slot within order not shifted correctly", timeSlot.getStarts(), orderEnds);

        // Identical time slots
        timeSlot = restaurantService.getTimeSlot(restaurant, new GregorianCalendar(
                2000, 1, 1, 12, 0
        ), 120, now);
        Assert.assertEquals("Identical time slots not shifted correctly", timeSlot.getStarts(), orderEnds);

        // Time slot within order
        timeSlot = restaurantService.getTimeSlot(restaurant, new GregorianCalendar(
                2000, 1, 1, 11, 30
        ), 60, now);
        Assert.assertEquals("Time slot within order not shifted correctly", timeSlot.getStarts(), orderEnds);

        // Time slot contain order
        timeSlot = restaurantService.getTimeSlot(restaurant, new GregorianCalendar(
                2000, 1, 1, 13, 0
        ), 240, now);
        Assert.assertEquals("Time slot contain order not shifted correctly", timeSlot.getStarts(), orderEnds);

        // Shift by multiple order but before last
        // Little messed up order to check SQL query ordering
        // Attention!!! - tight fit (minute to minute)
        Order beforeLastOrder = new Order();
        beforeLastOrder.setRestaurant(restaurant);
        beforeLastOrder.setStartsAt(new GregorianCalendar(2000, 1, 1, 13, 0));
        beforeLastOrder.setFinishedAt(new GregorianCalendar(2000, 1, 1, 14, 0));
        orderService.saveOrder(beforeLastOrder);

        order = new Order();
        order.setRestaurant(restaurant);
        order.setStartsAt(new GregorianCalendar(2000, 1, 1, 12, 0));
        order.setFinishedAt(new GregorianCalendar(2000, 1, 1, 13, 0));
        orderService.saveOrder(order);

        order = new Order();
        order.setRestaurant(restaurant);
        order.setStartsAt(new GregorianCalendar(2000, 1, 1, 15, 0));
        order.setFinishedAt(new GregorianCalendar(2000, 1, 1, 16, 0));
        orderService.saveOrder(order);


        timeSlot = restaurantService.getTimeSlot(restaurant, new GregorianCalendar(
                2000, 1, 1, 11, 0
        ), 60, now);
        Assert.assertEquals("Multiple order but before last not shifted correctly", timeSlot.getStarts(), beforeLastOrder.getFinishedAt());
    }
}
