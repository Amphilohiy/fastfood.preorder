import fastfood.preorder.dao.RestaurantDao;
import fastfood.preorder.db.Order;
import fastfood.preorder.db.Restaurant;
import fastfood.preorder.service.OrderService;
import fastfood.preorder.service.RestaurantService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.time.LocalTime;
import java.util.GregorianCalendar;
import java.util.List;

@RunWith(BlockJUnit4ClassRunner.class)
@Transactional
public class RestaurantDaoTest {
    private RestaurantService restaurantService = new RestaurantService();
    private OrderService orderService = new OrderService();
    private Restaurant restaurant;

    private RestaurantDao restaurantDao = new RestaurantDao();

    @Before
    public void initRestaurant(){
        restaurant = new Restaurant();
        restaurant.setAddress("Фрунзе, 67/1 к1 киоск");
        restaurant.setOpensAt(LocalTime.of(7, 0));
        restaurant.setClosesAt(LocalTime.of(21, 0));
        restaurantService.saveRestaurant(restaurant);
    }

    @Test
    public void findOrdersForDay(){
        GregorianCalendar now = new GregorianCalendar(2000, 1, 1, 0, 0);

        // First order for day
        Order firstOrder = new Order();
        firstOrder.setRestaurant(restaurant);
        firstOrder.setStartsAt(new GregorianCalendar(2000, 1, 1, 7, 0));
        firstOrder.setFinishedAt(new GregorianCalendar(2000, 1, 1, 8, 0));
        orderService.saveOrder(firstOrder);

        // Messing up creation order
        // Third order for day
        Order thirdOrder = new Order();
        thirdOrder.setRestaurant(restaurant);
        thirdOrder.setStartsAt(new GregorianCalendar(2000, 1, 1, 11, 0));
        thirdOrder.setFinishedAt(new GregorianCalendar(2000, 1, 1, 12, 0));
        orderService.saveOrder(thirdOrder);

        // Second order for day
        Order secondOrder = new Order();
        secondOrder.setRestaurant(restaurant);
        secondOrder.setStartsAt(new GregorianCalendar(2000, 1, 1, 9, 0));
        secondOrder.setFinishedAt(new GregorianCalendar(2000, 1, 1, 10, 0));
        orderService.saveOrder(secondOrder);

        // Orders for other day should be excluded from query
        // Order day before
        Order beforeOrder = new Order();
        beforeOrder.setRestaurant(restaurant);
        beforeOrder.setStartsAt(new GregorianCalendar(2000, 1, 0, 8, 0));
        beforeOrder.setFinishedAt(new GregorianCalendar(2000, 1, 0, 9, 0));
        orderService.saveOrder(beforeOrder);

        // Order day after
        Order afterOrder = new Order();
        afterOrder.setRestaurant(restaurant);
        afterOrder.setStartsAt(new GregorianCalendar(2000, 1, 2, 8, 0));
        afterOrder.setFinishedAt(new GregorianCalendar(2000, 1, 2, 9, 0));
        orderService.saveOrder(afterOrder);

        List<Order> ordersForDay = restaurantDao.findOrdersForDay(restaurant, now);
        Assert.assertEquals("Orders amount mismatch!", 3, ordersForDay.size());

        Assert.assertEquals("First order is not in order", ordersForDay.get(0).getId(), firstOrder.getId());
        Assert.assertEquals("First order is not in order", ordersForDay.get(1).getId(), secondOrder.getId());
        Assert.assertEquals("First order is not in order", ordersForDay.get(2).getId(), thirdOrder.getId());
    }
}
